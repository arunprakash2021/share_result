import time
import pandas as pd
from datetime import datetime
import excel_read_operation
import email_sender
from selenium import webdriver
from selenium.webdriver.support.ui import Select

testfile_loc = "TestCase/testfile_loc.xlsx"
chrome_path = r"chromedriver.exe"
reading_sheet = "Sheet1"


def result_pass():
    result = "Pass"
    remarks = ""
    return result, remarks


def result_fail(val):
    result = "Fail"
    remarks = val
    return result, remarks


def read_excel():
    site_name = ''
    reader = pd.read_excel(testfile_loc, sheet_name=reading_sheet)
    for row, column in reader.iterrows():
        sn = column["SN"]
        flag = column['execute_flag']
        Test_summary = column['Test_summary']
        xpath = column['xpath']
        action = column['action']
        value = column['value']
        if flag == "Yes":
            action_def(sn, Test_summary, xpath, action, value)
        elif flag == "No":
            print(sn, Test_summary, xpath, action, value)
        if action == 'open_url':
            site_name = value
    return site_name


def open_browser(value):
    global driver
    try:
        if value == 'Chrome':
            driver = webdriver.Chrome(chrome_path)
            driver.maximize_window()
        result, remarks = result_pass()
    except Exception as open_error:
        result, remarks = result_fail(open_error)
    return result, remarks


def open_url(value):
    try:
        driver.get(value)
        result, remarks = result_pass()
    except Exception as url_error:
        result, remarks = result_fail(url_error)
    return result, remarks


def click(xpath):
    try:
        driver.find_element_by_xpath(xpath).click()
        result, remarks = result_pass()
    except Exception as click_error:
        result, remarks = result_fail(click_error)
    return result, remarks


def send_value(xpath, value):
    try:
        driver.find_element_by_xpath(xpath).send_keys(value)
        result, remarks = result_pass()
    except Exception as input_error:
        result, remarks = result_fail(input_error)
    return result, remarks


def select_dropdown(xpath, value):
    try:
        val = driver.find_element_by_xpath(xpath)
        Select(val).select_by_visible_text(value)
        result, remarks = result_pass()
    except Exception as select_error:
        result, remarks = result_fail(select_error)
    return result, remarks


def wait(value):
    try:
        time.sleep(value)
        result, remarks = result_pass()
    except Exception as wait_error:
        result, remarks = result_fail(wait_error)
    return result, remarks


def verify_text(xpath, value, Test_summary):
    output_text = driver.find_element_by_xpath(xpath).text

    if xpath == "//title":
        output_text = driver.title
    try:
        assert value in output_text
        result, remarks = result_pass()
        result1 = "Allotted"
    except AssertionError as v_text:
        remarks = "Actual value is " + output_text + " Input value is " + value + " Not Matched"
        print(remarks)
        result, remarks = result_fail(v_text)
        result1 = "Not Allotted"
        output_text = driver.find_element_by_xpath(xpath+'[2]').text
    excel_read_operation.write_ipo_excel(Test_summary, result1, output_text)
    return result, remarks


def exit_window():
    try:
        driver.quit()
        result, remarks = result_pass()
    except Exception as exit_error:
        result, remarks = result_fail(exit_error)
    return result, remarks


def close_window():
    try:
        driver.close()
        result, remarks = result_pass()
    except Exception as close_error:
        result, remarks = result_fail(close_error)
    return result, remarks


def clear_window(xpath):
    try:
        driver.find_element_by_xpath(xpath).clear()
        result, remarks = result_pass()
    except Exception as clear_error:
        result, remarks = result_fail(clear_error)
    return result, remarks


def action_def(sn, Test_summary, xpath, action, value):
    result = ""
    remarks = ""
    try:
        if action == 'open_browser':
            result, remarks = open_browser(value)
        elif action == 'open_url':
            result, remarks = open_url(value)
        elif action == 'click':
            result, remarks = click(xpath)
        elif action == 'send_value':
            result, remarks = send_value(xpath, value)
        elif action == 'select_dropdown':
            result, remarks = select_dropdown(xpath, value)
        elif action == 'wait':
            result, remarks = wait(value)
        elif action == 'compare':
            result, remarks = verify_text(xpath, value, Test_summary)
        elif action == 'exit':
            result, remarks = exit_window()
        elif action == 'close':
            result, remarks = close_window()
        elif action == 'clear':
            result, remarks = clear_window(xpath)
        else:
            result, remarks = result_fail("Action not supported by framework")
            print("Action not supported by framework")
    except Exception as es:
        print(es)
    print(sn, Test_summary, result, remarks)
    excel_read_operation.write_excel(sn, Test_summary,  result, remarks)


if __name__ == "__main__":
    try:
        started_time = datetime.now()
        excel_read_operation.remove_file()
        excel_read_operation.write_header()
        site = read_excel()
        excel_read_operation.write_summary(site, started_time)
        email_sender.send_selenium_report("Ipo Result")

    except Exception as e:
        print(e)
